package com.test.wordcount;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable ;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

public class WordCount {
	
	private static Text word=new Text();
	private static LongWritable one=new LongWritable(1);
	private static final String DELIMITER=",";
	
	
	public static class Map extends Mapper<LongWritable, Text, Text, LongWritable>{

		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, LongWritable>.Context context)
				throws IOException, InterruptedException {
			 String line=value.toString();
			    StringTokenizer token=new StringTokenizer(line,DELIMITER);
			    while(token.hasMoreElements()){
			    	   word.set(token.nextToken());
			    	   context.write(word,one);		    	
			    }
		}

		
		
		
	}
	
	public static class Reduce extends  Reducer<Text, LongWritable, Text, LongWritable> {

		@Override
		protected void reduce(Text key, Iterable<LongWritable> values,
				Reducer<Text, LongWritable, Text, LongWritable>.Context context) throws IOException, InterruptedException {
			
			Long sum=0L;
			     for(LongWritable val:values) {
			    	 
			    	 sum+=val.get();
			     }
			     context.write(key, new LongWritable(sum));
			
		}

		
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
